/*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- - */

var restify = require('restify');
var builder = require('botbuilder');
var request = require('request');
var locationDialog = require('botbuilder-location');



//=========================================================
// Bot Setup
//=========================================================

// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function() {
  console.log('%s listening to %s', server.name, server.url);
});

// Create chat bot
var connector = new builder.ChatConnector({
  appId: 'd82f74b6-5f0d-428e-9a37-fce3b90ce877', //process.env.MICROSOFT_APP_ID,
  appPassword: 'geLWqhteBGesJyFHWnTSXdi' //process.env.MICROSOFT_APP_PASSWORD
});

var bot = new builder.UniversalBot(connector);
server.post('/api/messages', connector.listen());
var model = 'https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/272bd0e2-1506-4520-a015-668e3f83e22a?subscription-key=de29af4b35b8471cb4de632d0c7f1e8f&verbose=true';
var recognizer = new builder.LuisRecognizer(model);
var dialog = new builder.IntentDialog({
  recognizers: [recognizer]
});
bot.dialog('/', dialog);

bot.on('conversationUpdate', function(message) {

  if (message.membersAdded) {
    var iterateMembers = function(identity) {
      if (identity.id === message.address.bot.id) {
        var reply = new builder.Message()
          .address(message.address)
          .text('Hi! I am the LUIS news bot. Tell me about a news topic you want to find!');
        bot.send(reply);
      }
    };
    message.membersAdded.forEach(iterateMembers);
  }
});




dialog.matches('News', [
  function(session, args, next) {

    var positiveSentimentEntity = builder.EntityRecognizer.findEntity(args.entities, 'PositiveSentiment');
    var negativeSentimentEntity = builder.EntityRecognizer.findEntity(args.entities, 'NegativeSentiment');
    var newsTopicEntity = builder.EntityRecognizer.findEntity(args.entities, 'NewsTopic');

    if (newsTopicEntity !== undefined && newsTopicEntity !== null) {

      var newsTopic = newsTopicEntity.entity;
      // chain promises!!! Episode IV
      // Search news then
      // retrurn 10 news objects
      // every new object has data
      // then pass descriptions from the promise
      // so getSentimentScore
      // okay
      // after it returns
      // then pass the score to
      var session.dialogData.expectedCount;
      var searchNews = function(news_topic) {
        return new Promise(function(resolve, reject) {
          var options = {
            url: 'https://api.cognitive.microsoft.com/bing/v5.0/news/search?q=' + news_topic,
            method: 'GET',
            headers: {
              'Ocp-Apim-Subscription-Key': 'e10255404c29447eae3d6d845b1edc45'
            }
          };
          request(options,
            function(error, response, body) {
              if (!error) {
                session.dialogData.expectedCount = JSON.parse(body).value.length;
                resolve(body);
              } else {
                console.log('there s been an error in the search');
              }
            });
        });
      };

      var getSentimentScore = function(article_description) {
        return new Promise(function(resolve, reject) {
          var options = {
            url: 'https://westus.api.cognitive.microsoft.com/text/analytics/v2.0/sentiment',
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Ocp-Apim-Subscription-Key': 'f37f889d9542470286db46977039531b'
            },
            body: JSON.stringify({
              "documents": [{
                "language": "en",
                "id": "1",
                "text": article_description
              }]
            }),
          };
          request(options,
            function(error, response, body) {
              if (!error)
                resolve(JSON.parse(body).documents[0].score);
              else
                console.log(error);
            });
        });
      };

      searchNews(newsTopic)
        .then(function(body) {
          var attachmentsCards = [];
          JSON.parse(body).value.forEach(function(currentValue, index, array) {


            getSentimentScore(currentValue.description).then(function(score) {
              if (+score > 0.5) {
                attachmentsCards.push(new builder.HeroCard(session)
                  .title(currentValue.name + " " + score)
                  .text(currentValue.description)
                  .buttons([builder.CardAction.openUrl(session, currentValue.url, "Link")])
                );
              } else {
                session.dialogData.expectedCount--;
              }
              if (attachmentsCards.length === session.dialogData.expectedCount) {
                if (session.dialogData.expectedCount === 0) {
                  session.endDialog("Please try another search query");
                } else {
                  var msg = new builder.Message(session)
                    .textFormat(builder.TextFormat.xml)
                    .attachmentLayout(builder.AttachmentLayout.carousel)
                    .attachments(attachmentsCards);
                  session.endDialog(msg); //session.endDialog(msg)
                }
              }

            });

          });

        });
    }
  }
]);
